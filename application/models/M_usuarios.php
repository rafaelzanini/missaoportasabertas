<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_usuarios extends CI_Model {

    public function validate() {
        $this->db->where('login', $this->input->post('username')); 
        $this->db->where('senha', md5($this->input->post('password')));

		$query = $this->db->get('usuarios');
        if ($query->num_rows() == 1) { 
            return true;
        }
		return false;
    }
	
    function logged() {
        $logged = $this->session->userdata('logged');

        if (!isset($logged) || $logged != true) {
            echo 'Voce nao tem permissao para entrar nessa pagina. <a href="login">Efetuar Login</a>';
            die();
        }
    }
}