<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_livros extends CI_Model {
	
	public function store($dados = null, $id = null) {
		
		if ($dados) {
			if ($id) {
				$this->db->where('id', $id);
				if ($this->db->update("livros", $dados)) {
					return true;
				} else {
					return false;
				}
			} else {
				if ($this->db->insert("livros", $dados)) {
					return true;
				} else {
					return false;
				}
			}
		}		
	}

	public function get($id = null){		
		if ($id) {
			$this->db->where('id', $id);
		}
		$this->db->order_by("id", 'asc');
		return $this->db->get('livros');
	}

	public function search($fields){
		if (isset($fields['nome'])) {
			$nome = $fields['nome'];
			$this->db->where("nome LIKE '%$nome%'");
		}
		$this->db->order_by("id", 'asc');
		return $this->db->get('livros');
	}
	
	public function delete($id = null){
		if ($id) {
			return $this->db->where('id', $id)->delete('livros');
		}
	}

}
