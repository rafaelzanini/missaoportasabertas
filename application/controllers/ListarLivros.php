<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ListarLivros extends CI_Controller {
	public function index()
	{
		$this->load->library('session');
		$session = $this->session->get_userdata();
		if ($session['logged']) {		
			if ($session['mensagem']) {
				$variaveis['mensagem'] = $session['mensagem'];
				$data = array(
					'mensagem' => false
                );
                $this->session->set_userdata($data);
			}
			$variaveis['livros'] = $this->m_livros->get();
			$this->load->view('include/topo', $variaveis);
			$this->load->view('livros/listar', $variaveis);
			$this->load->view('include/rodape', '');
		} else {
			redirect('login');
		}
	}
	
	public function pesquisar()
	{
		$this->load->library('session');
		$session = $this->session->get_userdata();
		if ($session['logged']) {		
			if ($session['mensagem']) {
				$variaveis['mensagem'] = $session['mensagem'];
				$data = array(
					'mensagem' => false
                );
                $this->session->set_userdata($data);
			}
			$pesquisa = [];
			if ($this->input->post('nome')) {
				$pesquisa['nome'] = $this->input->post('nome');
			}
			
			$variaveis['livros'] = $this->m_livros->search($pesquisa);
			$this->load->view('include/topo', $variaveis);
			$this->load->view('livros/listar', $variaveis);
			$this->load->view('include/rodape', '');
		} else {
			redirect('login');
		}
	}
}
