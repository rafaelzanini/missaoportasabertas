<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');


error_reporting(E_ALL);
ini_set('display_errors', 1);

class Login extends CI_Controller {

    function __construct() {
        parent::__construct();
		$this->load->library('session');
    }

    function index() {
		
        $this->load->library('form_validation');
        $this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');
        $this->form_validation->set_error_delimiters('<p class="error">', '</p>');

        $this->load->model('m_usuarios', 'usuarios');		
        $query = $this->usuarios->validate();

        if ($this->form_validation->run() == FALSE) {
            $this->load->view('login/login_view2');
        } else {
            if ($query) {
                $data = array(
                    'login' => $this->input->post('username'),
                    'logged' => true,
					'mensagem' => false
                );
                $this->session->set_userdata($data);
				redirect('listarLivros');
            } else {		
                redirect('login','refresh');
            }
        }
    }
	
	function logout() {
		$this->session->sess_destroy();
		redirect('login','refresh');
    }
}