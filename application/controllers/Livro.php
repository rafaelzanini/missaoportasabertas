<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Livro extends CI_Controller {

	function __construct() {
        parent::__construct();
		$this->load->library('session');
		$session = $this->session->get_userdata();
		if (!$session['logged']) {
			redirect('login');
		}
    }
	
	public function create()
	{
		$variaveis['titulo'] = 'Novo Cadastro';
		$this->load->view('include/topo', '');
		$this->load->view('livros/cadastro', $variaveis);
		$this->load->view('include/rodape', '');
	}

	public function store(){
		$this->load->library('form_validation');
		
		$regras = array(
		        array(
		                'field' => 'nome',
		                'label' => 'Nome',
		                'rules' => 'required'
		        ),
		        array(
		                'field' => 'quantidade',
		                'label' => 'Quantidade',
		                'rules' => 'required'
		        ),
				array(
		                'field' => 'valor',
		                'label' => 'Valor',
		                'rules' => 'required'
		        ),
		);
		
		$this->form_validation->set_rules($regras);

		if ($this->form_validation->run() == FALSE) {
			$variaveis['titulo'] = 'Novo Usuário';
			$this->load->view('include/topo', '');
			$this->load->view('livros/cadastro', $variaveis);
			$this->load->view('include/rodape', '');
		} else {
			$id = $this->input->post('id');			
			$dados = array(			
				"nome" => $this->input->post('nome'),
				"quantidade" => $this->input->post('quantidade'),
				"valor" => $this->input->post('valor')
			);
			
			if ($this->m_livros->store($dados, $id)) {
				$variaveis['mensagem'] = "Dados gravados com sucesso!";				
			} else {
				$variaveis['mensagem'] = "Ocorreu um erro. Por favor, tente novamente.";
			}
			$data = array('mensagem' => $variaveis['mensagem']);
			$this->session->set_userdata($data);
			redirect('listarlivros');		
		}
	}

	public function edit($id = null){		
		if ($id) {			
			$cadastros = $this->m_livros->get($id);			
			if ($cadastros->num_rows() > 0 ) {
				$variaveis['titulo'] = 'Edição de Livro';
				$variaveis['id'] = $cadastros->row()->id;
				$variaveis['nome'] = $cadastros->row()->nome;
				$variaveis['quantidade'] = $cadastros->row()->quantidade;
				$variaveis['valor'] = $cadastros->row()->valor;
				$this->load->view('include/topo', $variaveis);
				$this->load->view('livros/cadastro', $variaveis);
				$this->load->view('include/rodape', '');
			} else {
				$data = array('mensagem' => "Registro não encontrado.");
				$this->session->set_userdata($data);
				redirect('listarlivros');
			}			
		}		
	}

	public function delete($id = null) {
		if ($this->m_livros->delete($id)) {
			$data = array('mensagem' => "Registro excluído com sucesso!");
			$this->session->set_userdata($data);
			redirect('listarlivros');
			
		}
	}
}
