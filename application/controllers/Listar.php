<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Listar extends CI_Controller {
	public function index()
	{
		$this->load->library('session');
		$session = $this->session->get_userdata();
		if ($session['logged']) {
			if ($session['mensagem']) {
				$variaveis['mensagem'] = $session['mensagem'];
				$data = array(
					'mensagem' => false
                );
                $this->session->set_userdata($data);
			}
			$variaveis['cadastros'] = $this->m_cadastros->get();
			$this->load->view('include/topo', $variaveis);
			$this->load->view('usuarios/listar', $variaveis);
			$this->load->view('include/rodape', '');
		} else {
			redirect('login');
		}
	}	
}
