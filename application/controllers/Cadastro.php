<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cadastro extends CI_Controller {

    function __construct() {
        parent::__construct();
		$this->load->library('session');
		$session = $this->session->get_userdata();
		if (!$session['logged']) {
			redirect('login');
		}
    }
	
	public function create()
	{
		$variaveis['titulo'] = 'Novo Cadastro';
		$this->load->view('include/topo', '');
		$this->load->view('usuarios/cadastro', $variaveis);
		$this->load->view('include/rodape', '');
	}

	public function store(){		
		$this->load->library('form_validation');
		
		$regras = array(
		        array(
		                'field' => 'nome',
		                'label' => 'Nome',
		                'rules' => 'required'
		        ),
		        array(
		                'field' => 'login',
		                'label' => 'Login',
		                'rules' => 'required'
		        ),
				array(
		                'field' => 'senha',
		                'label' => 'Senha',
		                'rules' => 'required'
		        ),
				array(
		                'field' => 'senha2',
		                'label' => 'Confirmar Senha',
		                'rules' => 'required'
		        ),
		);
		
		$this->form_validation->set_rules($regras);

		if ($this->form_validation->run() == FALSE) {
			$variaveis['titulo'] = 'Novo Usuário';
			$this->load->view('include/topo', '');
			$this->load->view('usuarios/cadastro', $variaveis);
			$this->load->view('include/rodape', '');
		} else {
			$id = $this->input->post('id');			
			$dados = array(			
				"nome" => $this->input->post('nome'),
				"login" => $this->input->post('login'),
				"senha" => md5($this->input->post('senha'))
			);
			
			if ($this->m_cadastros->store($dados, $id)) {
				$variaveis['mensagem'] = "Dados gravados com sucesso!";				
			} else {
				$variaveis['mensagem'] = "Ocorreu um erro. Por favor, tente novamente.";
			}
			$data = array('mensagem' => $variaveis['mensagem']);
			$this->session->set_userdata($data);
			redirect('listar');		
		}
	}

	public function edit($id = null){		
		if ($id) {			
			$cadastros = $this->m_cadastros->get($id);			
			if ($cadastros->num_rows() > 0 ) {
				$variaveis['titulo'] = 'Edição de Registro';
				$variaveis['id'] = $cadastros->row()->id;
				$variaveis['nome'] = $cadastros->row()->nome;
				$variaveis['login'] = $cadastros->row()->login;
				$variaveis['senha'] = $cadastros->row()->senha;
				$this->load->view('include/topo', $variaveis);
				$this->load->view('usuarios/cadastro', $variaveis);
				$this->load->view('include/rodape', '');
			} else {
				$data = array('mensagem' => "Registro não encontrado.");
				$this->session->set_userdata($data);
				redirect('listar');
			}			
		}		
	}

	public function delete($id = null) {
		if ($this->m_cadastros->delete($id)) {
			$data = array('mensagem' => "Registro excluído com sucesso!");
			$this->session->set_userdata($data);
			redirect('listar');	
			
		}
	}
}
