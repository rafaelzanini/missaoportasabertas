	<div class="container">
		<h1 class="text-center">Lista de usu&aacute;rios</h1>
		<div class="col-md-12">
			<div class="row">
				<?php echo anchor('cadastro/create', 'Novo Usu&aacute;rio', array('class' => 'btn btn-success')); ?>
			</div>
			<div class="row">
				<h3><?php echo $cadastros->num_rows(); ?> registros(s)</h3>
			</div>
			<div class="row">
			<?php if ($cadastros->num_rows() > 0): ?>
				<table class="table table-striped">
					<thead>
						<tr>
							<th>Código</th>
							<th>Nome</th>
							<th>Login</th>
							<th>Ações</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach($cadastros->result() as $cadastro): ?>
						<tr>
							<td><?php echo $cadastro->id ?></td>
							<td><?php echo $cadastro->nome ?></td>
							<td><?php echo $cadastro->login ?></td>
							<td><?php echo anchor("cadastro/edit/$cadastro->id", "Editar") ?>
							<a href="#" class='confirma_exclusao' data-id="<?php echo $cadastro->id ?>" data-nome="<?php echo $cadastro->nome ?>" />Excluir</a></td>
						</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
				<?php else: ?>
					<h4>Nenhum registro cadastrado.</h4>
				<?php endif; ?>
			</div>
		</div>	
	</div>
<div class="modal fade" id="modal_confirmation">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Confirmação de Exclusão</h4>
      </div>
      <div class="modal-body">
        <p>Deseja realmente excluir o registro <strong><span id="nome_exclusao"></span></strong>?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Não</button>
        <button type="button" class="btn btn-danger" id="btn_excluir">Sim</button>
      </div>
    </div>
  </div>
</div>