<h1 class="text-center"><?= $titulo ?></h1>
<div class="col-md-6 col-md-offset-3">
	<div class="row">
		<?= form_open('cadastro/store')  ?>
			<div class="form-group">
				<label for="nome">Nome</label><span class="erro"><?php echo form_error('nome') ?  : ''; ?></span>
				<input type="text" name="nome" id="nome" class="form-control" value="<?= set_value('nome') ? : (isset($nome) ? $nome : '') ?>" autofocus='true' />
			</div>
			<div class="form-group">
				<label for="login">Login</label><span class="erro"><?php echo form_error('login') ?  : ''; ?></span>
				<input type="text" name="login" id="login" class="form-control" value="<?= set_value('login') ? : (isset($login) ? $login : '') ?>" autofocus='true' />
			</div>
			<div class="form-group">
				<label for="senha">Senha</label><span class="erro"><?php echo form_error('senha') ?  : ''; ?></span>
				<input type="password" name="senha" id="senha" class="form-control" value="<?= set_value('senha') ? : (isset($senha) ? $senha : '') ; ?>" />
			</div>
			<div class="form-group">
				<label for="senha2">Confirmar senha</label><span class="erro"><?php echo form_error('senha2') ?  : ''; ?></span>
				<input type="password" name="senha2" id="senha2" class="form-control" value="<?= set_value('senha') ? : (isset($senha) ? $senha : '') ; ?>" />
			</div>
			<div class="form-group text-right">
				<input type="submit" value="Salvar" class="btn btn-success" />
			</div>
			<input type='hidden' name="id" value="<?= set_value('id') ? : (isset($id) ? $id : ''); ?>">
		<?= form_close(); ?>
	</div>
	<div class="row"><hr></div>
</div>	
