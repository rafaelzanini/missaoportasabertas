<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Missao portas abertas</title>
	<?php echo link_tag('assets/bootstrap/css/bootstrap.min.css') ?>
	<?php echo link_tag('assets/bootstrap/css/bootstrap-theme.min.css') ?>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.16/datatables.min.css"/>
	<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.16/datatables.min.js"></script>
	<link rel="stylesheet" type="text/css" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css" />
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>

	<style>
		.erro {
			color: #f00;
		}
	</style>
</head>
<body>
<?php

defined('BASEPATH') OR exit('No direct script access allowed');
?>
<nav class="navbar navbar-inverse" style="background-image: linear-gradient(to bottom,#1b5a7c 0,#222 100%) !important; border-radius: 1px !important;">
    <div class="container-fluid">
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Menu<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="<?php echo base_url();?>listar">Usuários</a></li>
                        <li><a href="<?php echo base_url();?>listarlivros">Biblioteca</a></li>
                    </ul>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li class=""><a href="login/logout">Sair</a></li>
            </ul>
        </div>
    </div>
</nav>
<div class="container">
	<?php if (isset($mensagem)): ?>
	<div class="col-md-12" id="mensagem">
		<div class="row">
			<div class="alert alert-success text-center">
				<?php echo $mensagem; ?>
			</div>
		</div>
	</div>	
	<?php endif; ?>