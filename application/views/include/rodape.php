</div>
<script src="<?php echo base_url('assets/js/jquery.js') ?>"></script>	
<script src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.maskMoney.js') ?>"></script>	
	<script>	
		var base_url = "<?= base_url(); ?>";	
		$(function(){		
			setTimeout(function () {
				$('#mensagem').hide();
			}, 2000);
			$("#valor").maskMoney();
			$('.confirma_exclusao').on('click', function(e) {
			    e.preventDefault();
			    
			    var nome = $(this).data('nome');
			    var id = $(this).data('id');
			    
			    $('#modal_confirmation').data('nome', nome);
			    $('#modal_confirmation').data('id', id);
			    $('#modal_confirmation').modal('show');
			});
			
			$('#modal_confirmation').on('show.bs.modal', function () {
			  var nome = $(this).data('nome');
			  $('#nome_exclusao').text(nome);
			});	
			
			$('#btn_excluir').click(function(){
				var url = window.location;
				if (url == 'http://portasabertas/listarlivros' || url == 'http://portasabertas/listarlivros/pesquisar') {
					var controller = 'livro';
				} else {
					var controller = 'cadastro';
				}
				
				var id = $('#modal_confirmation').data('id');
				document.location.href = base_url + "index.php/"+controller+"/delete/"+id;
			});
		});
	</script>
</body>
</html>