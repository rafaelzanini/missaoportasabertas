<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<link rel="stylesheet" type="text/css" href="<?=base_url('assets/css/login.css')?>">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<body class="main">
<div class="login-screen"></div>
    <div class="login-center">
        <div class="container min-height" style="margin-top: 20px;">
        	<div class="row">
                <div class="col-xs-4 col-md-offset-8">
                    <div class="login" id="card">
                    	<div class="front signin_form"> 
                        <p>Login</p>
                          <form action="" method="post" class="login-form">						  
                              <div class="form-group">
                                  <div class="input-group">
                                      <input type="text" name="username" class="form-control" placeholder="Entre com seu Login">
                                      <span class="input-group-addon">
                                          <i class="glyphicon glyphicon-user"></i>
                                      </span>
                                  </div>
                              </div>
                              <div class="form-group">
                                  <div class="input-group">
                                      <input type="password" name="password" class="form-control" placeholder="Entre com sua senha">
                                      <span class="input-group-addon">
                                          <i class="glyphicon glyphicon-lock"></i>
                                      </span>
                                  </div>
                              </div>
                              <div class="checkbox">
								<label>
									<input type="checkbox">Reelembrar senha no próximo acesso.
								</label>
                              </div>                              
                              <div class="form-group sign-btn">
                                  <input type="submit" class="btn" value="Acessar sistema">
                              </div>
                          </form>
                        </div>
                        <div class="back signup_form" style="opacity: 0;">                         
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jQuery-Flip/1.0.18/jquery.flip.js"></script>
    
    </body>