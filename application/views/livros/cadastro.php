<h1 class="text-center"><?= $titulo ?></h1>
<div class="col-md-6 col-md-offset-3">
	<div class="row">
		<?= form_open('livro/store')  ?>
			<div class="form-group">
				<label for="nome">Nome</label><span class="erro"><?php echo form_error('nome') ?  : ''; ?></span>
				<input type="text" name="nome" id="nome" class="form-control" value="<?= set_value('nome') ? : (isset($nome) ? $nome : '') ?>" autofocus='true' />
			</div>
			<div class="form-group">
				<label for="quantidade">Qtd.</label><span class="erro"><?php echo form_error('quantidade') ?  : ''; ?></span>
				<input type="number" name="quantidade" id="quantidade" class="form-control" value="<?= set_value('quantidade') ? : (isset($quantidade) ? $quantidade : '') ?>" autofocus='true' />
			</div>			
			<div class="form-group">
				<label for="qtd">Valor</label><span class="erro"><?php echo form_error('valor') ?  : ''; ?></span>
				<input type="text" name="valor" id="valor" class="form-control" value="<?= set_value('valor') ? : (isset($valor) ? number_format($valor,2,',','.'): '') ?>" autofocus='true' />
			</div>
			<div class="form-group text-right">
				<input type="submit" value="Salvar" class="btn btn-success" />
			</div>
			<input type='hidden' name="id" value="<?= set_value('id') ? : (isset($id) ? $id : ''); ?>">
		<?= form_close(); ?>
	</div>
	<div class="row"><hr></div>
</div>	
