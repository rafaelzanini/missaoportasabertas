	<div class="container">
		<h1 class="text-center">Lista de Livros</h1>
		<div class="col-md-12">
			<div class="row" style="float: left">
				<?php echo anchor('livro/create', 'Novo Livro', array('class' => 'btn btn-success')); ?>
			</div>
			<div class="row" style="float: right">
			
			<form action="/listarlivros/pesquisar" method="post">
			  <div class="form-inline">
				<input type="text" class="form-control" id="nome" name="nome" placeholder="Nome do Livro">
				<input type=submit value='Pesquisar' />
			  </div>
			</form>
			</div>
			<div class="row">
			<?php if ($livros->num_rows() > 0): ?>
				<table class="table table-striped">
					<thead>
						<tr>
							<th>Código</th>
							<th>Nome</th>
							<th>Qtd.</th>
							<th>Valor</th>
							<th>Ações</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach($livros->result() as $livro): ?>
						<tr>
							<td><?php echo $livro->id ?></td>
							<td><?php echo $livro->nome ?></td>
							<td><?php echo $livro->quantidade ?></td>
							<td><?php echo number_format($livro->valor,2,',','.') ?></td>
							<td><?php echo anchor("livro/edit/$livro->id", "Editar") ?>
							<a href="#" class='confirma_exclusao' data-id="<?php echo $livro->id ?>" data-nome="<?php echo $livro->nome ?>" />Excluir</a></td>
						</tr>
						<?php endforeach; ?>
					</tbody>
				</table>										
				<?php endif; ?>
			</div>
		</div>
		<?php if ($livros->num_rows() == 0): ?>
			<div class="row" style="float: left">
				<h4>Nenhum registro cadastrado.</h4>
			</div>
		<?php endif; ?>					
		<div class="row" style="float: right">
				<h4><?php echo $livros->num_rows(); ?> Livros(s)</h4>
			</div>
	</div>
<div class="modal fade" id="modal_confirmation">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Confirmação de Exclusão</h4>
      </div>
      <div class="modal-body">
        <p>Deseja realmente excluir o registro <strong><span id="nome_exclusao"></span></strong>?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Não</button>
        <button type="button" class="btn btn-danger" id="btn_excluir">Sim</button>
      </div>
    </div>
  </div>
</div>