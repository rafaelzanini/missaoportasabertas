--
-- Database: `portasabertas`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(32) NOT NULL,
  `nome` varchar(255) DEFAULT NULL,
  `senha` varchar(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8

--
-- Estrutura da tabela `livros`
--
CREATE TABLE `livros` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(32) NOT NULL,
  `quantidade` int(11) DEFAULT NULL,
  `valor` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=utf8

--
-- Usuário Admin 
--
INSERT INTO `portasabertas`.`usuarios`
(`id`,`login`,`nome`,`senha`)
VALUES
(1,'admin','admin','10ed1697617fe7758b4236d5b791286c')
